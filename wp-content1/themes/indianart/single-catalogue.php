<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="content_block_background" id="cbb">
    <h2 class="page-title"><?php the_title(); ?></h2>
    <div class="wrap">
        <div id="primary" class="content-area">
            <div id="content" class="site-content" role="main">
    
                <?php /* The loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>
    
                  <?php the_title(); ?>
                  <div class="imgg">
                  <?php
                    if ( has_post_thumbnail()):
                        the_post_thumbnail('catalogue-featured-image');
                    endif;
                    ?> 
                  
                  <?php //echo do_shortcode('[easy_image_gallery]'); ?>
            </div>
            
            <div class="cat_cont">
                <?php the_content(); ?>
            </div>
                <?php endwhile; ?>
    
            </div><!-- #content -->
        </div><!-- #primary -->
	</div>
</section>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>