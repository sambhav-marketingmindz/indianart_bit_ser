<?php
/**
 * 
 * Template name: Inform Page
 *
 */
get_header();

?>   


    <section class="content_block_background" id="cbb">
        <h2 class="page-title"><?php the_title(); ?></h2>
            <section id="row-<?php the_ID(); ?>" class="content_block clearfix">
                <div class="row clearfix">
                    <div class="box two-three">
                    
                    	<p>Thank you for registering with us.<br>A verification email has been sent to your mentioned email id.</p>

                        
                    </div>
                </div>
            </section>
    </section>
<?php 
get_footer();
?>