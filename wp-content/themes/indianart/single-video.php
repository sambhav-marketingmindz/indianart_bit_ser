<?php
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */

get_header(); ?>
<section class="content_block_background"  id="cbb">
    <h2 class="page-title"><?php the_title(); ?></h2>
    <div class="wrap">
        <div id="primary" class="content-area">
            <div id="content" class="site-content" role="main">
    
                <?php /* The loop */ ?>
                <?php while ( have_posts() ) : the_post(); ?>
                	<div class="single-posts">
                    	<div class="single-posts_left">
                            <?php /*?><h2><?php the_title(); ?></h2><?php */?>
                            <?php $video = get_field('iframe_code'); echo $video; ?> 
                        </div>
                        <div class="single-posts_right">
                        	<?php the_content(); ?>
                        </div>
    					<div class="clear"></div>
                    </div>
                <?php endwhile; ?>
    
            </div><!-- #content -->
        </div><!-- #primary -->
	</div>
</section>
<?php //get_sidebar(); ?>
<?php get_footer(); ?>