<?php 
/*
**Template name: Events Page
*/
get_header();
?>
<section class="content_block_background" id="cbb">
	<h2 class="page-title"><?php the_title(); ?></h2>
	<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>
    <div class="wrap">
        <div class="boxs">
    		<h2><?php _e('Upcoming Events'); ?></h2>
			<?php if( have_rows('upcoming_events') ): ?>
			<?php while( have_rows('upcoming_events') ): the_row(); ?>
            <div class="box">
            	<ul>
                    <li>
                    	<img src="<?php the_sub_field('images'); ?>" alt="" />
					</li>
                </ul>
            </div>
            <?php endwhile; ?>
			<?php endif; ?>
            <div class="clear"></div>    
        </div>
        
        <div class="boxs">
    		<h2><?php _e('Recent Events'); ?></h2>
			<?php if( have_rows('recent_events') ): ?>
			<?php while( have_rows('recent_events') ): the_row(); ?>
            <div class="box">
                <ul>
                    <li>
                    	<img src="<?php the_sub_field('recent_events_images'); ?>" alt="" />
					</li>
                </ul>
            </div>
            <?php endwhile; ?>
			<?php endif; ?>
            <div class="clear"></div>    
        </div>
    </div>
</section>
<?php get_footer(); ?>