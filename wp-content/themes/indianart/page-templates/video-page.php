<?php
/**
 * Template name: Video Page
 */

get_header(); ?>
<section class="content_block_background" id="cbb">
    <h2 class="page-title"><?php the_title(); ?></h2>
        <div class="wrap">
			<?php 
                $args = array( 'post_type' => 'video', 'posts_per_page' => 10,'order'=>'ASC' );
                $loop = new WP_Query( $args );
                while ( $loop->have_posts() ) : $loop->the_post();
            ?>
             <div class="video_sec">
             <h1><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
             <div class="video_sec_part">
			 	<?php $video = get_field('iframe_code'); echo $video; ?>
             </div>
             </div>  
			<?php 
                endwhile;
            ?>
        </div>
</section>
<?php get_footer(); ?>