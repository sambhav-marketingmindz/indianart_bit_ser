<?php
/**
 * 
 * Template name: Thank you Page
 *
 */
get_header();

?>   

    <section class="content_block_background" id="cbb">
        <h2 class="page-title"><?php the_title(); ?></h2>
            <section id="row-<?php the_ID(); ?>" class="content_block clearfix">
                <div class="row clearfix">
                    <div class="thank-you">
                    
                    	<h1 >Thank you for contacting us. We will be in touch with you very soon.</h1>
                        
                    </div>
                </div>
            </section>
    </section>
<?php 
get_footer();
?>