<?php
/**
 * 
 * Template name: New sign up page
 *
 */
ob_start();
session_start();
get_header();

?>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
<script type= "text/javascript" src = "<?php bloginfo('template_url'); ?>/js/countries.js"></script>
<script>
/*$( document ).ready(function() {
   var code = Math.floor((Math.random() * 1000000) + 9);
   $("#code").val(code);
});
window.onload = function() {
 var myInput = document.getElementById('captchacode');
 myInput.onpaste = function(e) {
   e.preventDefault();
 }
}*/
</script>
<div class="msg-user-block">
<?php 

$city = '';
if(isset($_POST['submit'])){
		global $first_name, $last_name, $address, $city, $state,$country, $pincode, $zipcode, $phone, $mobile, $email, $password, $captchacode;
					////////// indian user registration code////////////////////////////
		$first_name   = sanitize_text_field($_POST['fname']);
		$last_name	= 	sanitize_text_field($_POST['lname']);
		$address	  =	sanitize_text_field($_POST['address']);
		$city		 =	sanitize_text_field($_POST['city']);
		$state		=	sanitize_text_field($_POST['state']);
		$country	  =	sanitize_text_field($_POST['country']);
		$pincode	  =	sanitize_text_field($_POST['pincode']);
		$zipcode	  =	sanitize_text_field($_POST['zipcode']);
		$phone		=	sanitize_text_field($_POST['phone']);
		$email		= 	sanitize_email($_POST['email']);
		$password	 = 	esc_attr($_POST['password']);
		$mobile	   = 	sanitize_text_field($_POST['mobile']);
		$captchacode  =	sanitize_text_field($_POST['captchacode']);
        $active    = 'inactive' ;        
        $errormail = '';
        $errorcaptcha = '';
		if (!preg_match("/^[[:alnum:]][a-z0-9_.-]*@[a-z0-9.-]+\.[a-z]{2,4}$/i", $email)) {
			$errormail="Enter valid email id";
		} elseif( email_exists($email) ){
			$errormail= 'Email id already exists';
			}		
		 if( $captchacode  == ' ' || $_SESSION["code"] !=  $captchacode  ){
		 	$errorcaptcha = "Wrong Captcha Code Entered";
		 }
		if($errormail == '' && $errorcaptcha == '' ){      
                complete_registration(
								$first_name, 
								$last_name, 
								$address, 
								$city, 
								$state,
								$country, 
								$pincode, 
								$zipcode,
								$phone, 
								$mobile, 
								$email, 
								$password,
								$captchacode,
								$active
						); 
////////////////// end code ////////////////////////////////////////
						$registered_user = get_user_by ( 'email', $_POST['email'] );
						$user_id = $registered_user->ID; 

						$uri = get_template_directory_uri().'/images/img.png';
						$img_url = '<img src="'.$uri.'">';
						$success_url =  home_url()."/verify/?success1=".$user_id;					//$success_url = echo site_url()."/success/"?$user_id;
						$mailMsg = "Hi <b>".ucfirst($_POST['fname']).",</b><br><br>Please Verify your Email Account"."<br>Email:".$_POST['email']."<br>Verify link.:
						<a href=". $success_url ."><b>Click here to verify your email account</b></a><br><br><br><br>". $img_url ."<br><b>Indian Art Furnitures Pvt. Ltd.</b><br>
Ph. :: +91 141 6510124 Fax: +91 141 277 1724<br><br>
<b style='text-align:justify;'>Disclaimer: ******************************************************</b><br>
This email (including any attachments) is intended for the sole use of<br>
the intended recipient/s and may contain material that is CONFIDENTIAL AND<br>
PRIVATE COMPANY INFORMATION. Any review or reliance by others or copying or<br>
distribution or forwarding of any or all of the contents in this message is<br>
STRICTLY PROHIBITED. If you are not the intended recipient, please contact<br>
the sender by email and delete all copies; your cooperation in this regard<br>
is appreciated.<br><b>**************************************************</b>";
						$subject = "Verify email id";
						//$headers = array();
						$headers = 'MIME-Version: 1.0' . "\r\n";
						$headers .= 'Content-type: text/html; charset=iso-8859-15' . "\r\n";
						$headers .= 'From: Indian Art <info@indianartfurnitures.com>' . "\r\n";
						$result = wp_mail($email, $subject, $mailMsg,$headers);
		                $url = home_url()."/inform/";
                        header('Location: ' . $url);      
                  }
}
function complete_registration($first_name, $last_name, $address, $city, $state, $country, $pincode, $zipcode, $phone, $mobile, $email, $password,$captchacode ,$active) {                                global $reg_errors, $wpdb;					
						$userdata = array(
							'user_email' 		=> $email,
							'user_login'		=> $email,
							'user_pass' 		 => $password,
							'user_url'		  => $mobile,
							'first_name'		=> $first_name,
							'last_name'		 => $last_name,
							'show_admin_bar_front' => false
						);
						$user = wp_insert_user( $userdata );	
						# save meta fields
						add_user_meta($user, "address", $address);
						add_user_meta($user, "city", $city);
						add_user_meta($user, "state", $state);
						add_user_meta($user, "country", $country);
						add_user_meta($user, "pincode", $pincode);
						add_user_meta($user, "zipcode", $zipcode);
						add_user_meta($user, "phone", $phone);
						add_user_meta($user, "mobile", $mobile);
						add_user_meta($user, "captchacode", $captchacode);
						add_user_meta($user, "wpduact_status", $active);

                        ?>
	                    <span class="msg-user_com"> <?php //echo "Please check your Email id for verification";?></span>
 	                    <?php   } ?>
 	                    </div>
<section class="content_block_background" id="cbb">
<h2 class="page-title"><?php the_title(); ?></h2>
<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>
	<section id="row-<?php the_ID(); ?>" class="content_block clearfix">
		<div class="row clearfix">
        	<div class="box three-three">      	
            	<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post" autocomplete="on" class="rt_form" id="regis_form" name="myForm" onsubmit=
                "return(validate());">
                <p class="form-input-custom">
						<label for="firstname"><?php _e('Full Name (*)'); ?></label>
						<input type="text" class="register_input_fields" name="fname" value="<?php echo $first_name; ?>">
						</p>
					<p class="form-input-custom">
						<label for="email"><?php _e('Email (*)'); ?></label><span><b class="error"><?php echo $errormail; ?></b></span>
						<input type="text" id="email" name="email" value="<?php echo $email; ?>">
					</p>					
					<p class="form-input-custom">
						<label for="password"><?php _e('Password (*)'); ?></label>
						<input type="password" id="password" name="password" value="">
					</p>
					 <p class="form-input-custom">
						<label for="cpassword"><?php _e('Confirm Password (*)'); ?></label>
						<input type="password" id="cpassword"   name="cpassword" value="">
					</p> 
					<p class="form-input-custom">
						<label for="address"><?php _e('Address (*)'); ?></label>
						<input type="text" class="register_input_fields"  name="address" value="<?php echo $address; ?>">
					</p>
					<p class="form-input-custom">
						<label for="city"><?php _e('City (*)'); ?></label>
						<input type="text" class="register_input_fields"  name="city" id="city" value="<?php echo $city; ?>">
					</p>
					<p class="form-input-custom">
						<label for="pincode"><?php _e('Pincode (*)'); ?></label>
						<input type="text" class="register_input_fields"  name="pincode" id="zip" value="<?php echo $pincode; ?>" onkeyup="getCityState()">
					</p>
                    <p class="form-input-custom">
						<label for="country"><?php _e('Country (*)'); ?></label>
                        <select id="country" name="country" class="country_list"  value="<?php echo $country; ?>"></select>
                    </p>
					<p class="form-input-custom">
						<label for="phone"><?php _e('Phone (*)'); ?></label>
						<input type="text" class="register_input_fields"  name="phone" value="<?php echo $phone; ?>">
					</p>
					<p class="form-input-custom" id="captcha-area">
                    <label for="captcha" class="captcha-label">Captcha *</label><span><b class="error"><?php echo $errorcaptcha; ?></b></span>
                        <img src="<?php  echo get_template_directory_uri(); ?>/captcha/captcha_code_file.php?rand=<?php echo rand(); ?>" id='captchaimg'>
                        <input type="text"  name="captchacode" value="" placeholder="Enter captcha here" >
                        <small class="captcha-text">Can't read the image? click <a href='javascript: refreshCaptcha();'>here</a> to refresh</small>
                     </p>
					<div class="cums_ccs">
                    	<input type="submit" class="submit-button" name="submit" value="Register" id="submit"/>
                    </div>
				 </form>
              </div>
		</div>
	</section>
</section> 
<script type="text/javascript">
populateCountries("country");
<!--// Form validation code.
  function validate()
  {
  	if( document.myForm.fname.value == "" )
	 {
		alert( "Please provide your First name!" );
		document.myForm.fname.focus() ;
		return false;
	 }
	 if( document.myForm.email.value == "" )
	 {
		alert( "Please provide your Email!" );
		document.myForm.email.focus() ;
		return false;
	 }
	 if( document.myForm.password.value == "" )
	 {
		alert( "Please provide your Password!" );
		document.myForm.password.focus() ;
		return false;
	 }
	 if( document.myForm.cpassword.value == "" )
	 {
		alert( "Please provide your Confirm Password!" );
		document.myForm.cpassword.focus() ;
		return false;
	 }
	 if( document.myForm.cpassword.value != document.myForm.password.value )
	 {
		alert( "Password mismatch" );
		document.myForm.cpassword.focus() ;
		return false;	
	 }
	 if( document.myForm.address.value == "" )
	 {
		alert( "Please provide your Address!" );
		document.myForm.address.focus() ;
		return false;
	 }
	 if( document.myForm.city.value == "" )
	 {
		alert( "Please provide your City!" );
		document.myForm.city.focus() ;
		return false;
	 }
	 if( document.myForm.pincode.value == "" )
	 {
		alert( "Please provide your Pincode!" );
		document.myForm.pincode.focus() ;
		return false;
	 }else if (!document.myForm.pincode.value.match(/^\d+$/) ){
		alert ("Please only enter numeric characters for Pincode");
		return false;
	}
	if( document.myForm.country.value == "-1" )
	 {
		alert( "Please provide your Country!" );
		return false;
	 }
	 if( document.myForm.phone.value == "" )
	 {
		alert( "Please provide your Phone!" );
		document.myForm.phone.focus() ;
		return false;
	 }
	 else if (!document.myForm.phone.value.match(/^\d+$/) ){
		alert ("Please only enter numeric characters for Phone Number");
		return false;
	}else if (!document.myForm.phone.value.match(/^\d{10}$/) )
	{
		alert("Not a valid Number please Enter 10 digit valid Number");
		return false;
	}else{
	 document.getElementById("regis_form").submit();
	 }
  }
//-->
</script> 
 <script language='JavaScript' type='text/javascript'>
		function refreshCaptcha()
		{
			var img = document.images['captchaimg'];
			img.src = img.src.substring(0,img.src.lastIndexOf("?"))+"?rand="+Math.random()*1000;
		}
    </script>
<?php get_footer(); ?>  