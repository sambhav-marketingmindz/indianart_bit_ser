<?php
/*
Template Name: Lost Password
*/
global $rt_sidebar_location;
get_header(); 
global $wpdb, $user_ID;

function tg_validate_url() {
	global $post;
	$page_url = esc_url(home_url( '' ));
}

$error = '';
if (!$user_ID) { //block logged in users

	if($_POST['reset_pass']){
		
		
		//We shall SQL escape the input
		$user_input = $wpdb->escape(trim($_POST['user_input']));
		
		if ( strpos($user_input, '@') ) {
			$user_data = get_user_by_email($user_input);
			if(empty($user_data) || $user_data->caps[administrator] == 1) { //delete the condition $user_data->caps[administrator] == 1, if you want to allow password reset for admins also
				$error = __('Invalid Email.');
				
			}
		}
		else {
			$user_data = get_userdatabylogin($user_input);
			if(empty($user_data) || $user_data->caps[administrator] == 1) { //delete the condition $user_data->caps[administrator] == 1, if you want to allow password reset for admins also
				$error = __('Invalid Email');
				
			}
		}
		
		$user_login = $user_data->user_login;
		$user_email = $user_data->user_email;
		
		if(empty($error)){
			//generate reset key
			//$key = $wpdb->get_var($wpdb->prepare("SELECT user_activation_key FROM $wpdb->users WHERE user_login = %s", $user_login));
			//if ( empty($key) ) {
				// Generate something random for a key...
				$key = wp_generate_password(20, false);
				// Now insert the new md5 key into the db
				$wpdb->update($wpdb->users, array('user_activation_key' => $key), array('user_login' => $user_login));
			//}	

				 $uri = get_template_directory_uri().'/images/img.png';
                 $img_url = '<img src="'.$uri.'">';


            $headers = 'MIME-Version: 1.0' . "\r\n";
            $headers .= 'Content-type: text/html; charset=iso-8859-15' . "\r\n";
          
          	$message  = __('<b>Someone requested that the password be reset for the following account:</b>') . "<br>";
			//$message .= get_option('siteurl') . "\r\n\r\n";
			$message .= sprintf(__('Username: %s'), $user_login) . "<br>";
			$message .= __('If this was a mistake, just ignore this email and nothing will happen.') . "\r\n\r\n";
			$message .= __('To reset your password, visit the following address:') . "<br>";
			$message .= network_site_url("recovery/?action=rp&key=$key&login=" . rawurlencode($user_login), 'login') . "\r\n";

			$message .= "<br><br><br>". $img_url."<br><b>Indian Art Furnitures Pvt. Ltd.</b><br>
            Ph. :: +91 141 6510124 Fax: +91 141 277 1724<br><br>

            <b style='text-align:justify;'>Disclaimer: ******************************************************</b><br>
            This email (including any attachments) is intended for the sole use of<br>
            the intended recipient/s and may contain material that is CONFIDENTIAL AND<br>
            PRIVATE COMPANY INFORMATION. Any review or reliance by others or copying or<br>
            distribution or forwarding of any or all of the contents in this message is<br>
            STRICTLY PROHIBITED. If you are not the intended recipient, please contact<br>
            the sender by email and delete all copies; your cooperation in this regard<br>
            is appreciated.</b><br>
            <b>******************************************************************</b>";
			
			if ( $message && !wp_mail($user_email, 'Password Reset Request', $message,$headers) ) {
				$success = __('Email failed to send for some unknown reason.');
			}
			else {
				$success = __('We have just sent you an email with Password reset instructions.');
				
			}
		}
}

?>
	<script>
		function vali(){	
				var x = document.forms["lostpasswordform"]["user_input"].value;
				var atpos=x.indexOf("@");
				var dotpos=x.lastIndexOf(".");
				if (x == null || x == ""){
					alert ("Enter an email");
					return false;
				}
				else if (atpos<1 || dotpos<atpos+2 || dotpos+2>=x.length){
					alert("Not a valid email");
					return false;
				}
		}
	</script>

	<section class="content_block_background" id="cbb">
			<h2 class="page-title"><?php the_title(); ?></h2>
			<section id="row-<?php the_ID(); ?>" class="content_block clearfix">
					<?php
						if(!empty($error)){
							echo '<p style="color:red;">';
							echo $error;
							echo '</p>';
						}
							
						if(!empty($success)){
							echo '<p class="forget-instr" style="color:green;">';
							echo $success;
							echo '</p>';
						}
					?>
				
					<div class="wrap pm_cumt">
						<div class="contact-info">
							<div class="contact-left">
								<form name="lostpasswordform" id="lostpasswordform" action="" method="post" onsubmit="return vali();">      
									<input type="text" name="user_input" id="user_input" class="repasstextfield" placeholder="Enter Your Email" value="" />
									<input type="submit" name="reset_pass" id="reset_pass" value="Send Reset Password Link" class="reset_password-button" /> 
									<input type="hidden" name="redirect_to" value="<?php echo home_url( '/exclusive-collection?success=reset') ?>" />
									<?php do_action('lost_password'); ?>
								</form>
							</div>
						</div>
					<div class="clear"></div>
					</div>
			</section>
	</section>
<?php get_footer(); } ?>