<?php
ob_start();
/**
 * Template name: Exclusive Product Page
 */

session_start();
ob_start();
global $post;
$post_int_id = "";
get_header(); ?>
<section class="content_block_background" id="cbb">
	<div id="page-heading">
		<h2 class="page-title"><?php the_title(); ?></h2>
		<?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>
	</div>
	
		<?php 
			$current_user = wp_get_current_user();
			$user_id = $current_user->ID;
			$approval = get_user_meta($user_id,'wpduact_status',true);
		?>

		<?php
			if( !is_user_logged_in() || $approval == "deny" || $approval == ""){
				if(!empty($_REQUEST['login']) &&($_REQUEST['login'] == 'failed') ){
					echo '<p style="color:red;">Invalid username or password</p>';
				}
				if(!empty($_REQUEST['login']) &&($_REQUEST['login'] == 'blank') ){
					echo '<p style="color:red;">Please fillout your username or password.</p>';
				}
				if(!empty($_REQUEST['login']) &&($_REQUEST['login'] == 'blank?login=failed') ){
					echo '<p style="color:red;">Invalid username or password</p>';
				}
				if(!empty($_REQUEST['register']) &&($_REQUEST['register'] == 'true')){
					echo '<p style="color:green;">You are successfully registered. Check your email and activate your account.</p>';
				}
				if(!empty($_REQUEST['success']) &&($_REQUEST['success'] == 'passrecovered')){
					echo '<p style="color:green;">Your password has been successfully updated.</p>';
				}
				if(!empty($_REQUEST['login']) &&($_REQUEST['login'] == 'required') ){
					echo '<p style="color:red;">You have to login first.</p>';
				}
				if(!empty($_REQUEST['reset']) &&($_REQUEST['reset'] == 'true') ){
					echo '<p style="color:green;">Please check your email address, we send a link to change password.</p>';
				}
				if(!empty($_REQUEST['activation']) &&($_REQUEST['activation'] == 'true') ){
					echo '<p style="color:green;">Your Account has been successfully Activated. Admin approval required before login. We will send you an email after approval.</p>';
					$nwuserID = $_REQUEST['uid'];
					update_user_meta($nwuserID,'user_select','approve');
					
				}
		?>

<div class="exclu_bfr_login">
	<div class="exclu_login">
    	<h2><?php _e('Registered Users'); ?></h2>
        <hr/>
        <?php
				$args = array(
					'echo'           => true,
					'redirect' => 	home_url( '/exclusive-collection/' ),
					'form_id'        => 'loginform',
					'label_username' => __( 'Username' ),
					'label_password' => __( 'Password' ),
					'label_log_in'   => __( 'Log In' ),
					'id_username'    => 'user_login',
					'id_password'    => 'user_pass',
					'remember' => true,
					'id_submit'      => 'wp-submit',
					'value_username' => ''
				); 
			wp_login_form( $args );
			?>
    </div>
    <div class="exclu_signup">
    	<h2><?php _e('New User'); ?></h2>
        <hr/>
        <p><?php _e('By creating an account you will be able to see our exclusive collection...')?></p>
        <a href="<?php echo get_page_link( get_page_by_title('Sign Up')) ?>"><button class="signupp"><?php _e('Sign Up'); ?></button></a>
    </div>
    <div class="clear"></div>
</div>
<?php } else { ?>
	<?php if ( is_user_logged_in() || $approval == "approve" ) { ?>
    <div class="wrap pm_cumt">
	        	<?php
					$terms = apply_filters( 'taxonomy-images-get-terms', '', array(
					'taxonomy' => 'excollection',
					'term_args' => array(
					'slug' => $term->slug,
					)
					) );
				?>
		<div class="excl-range-box">
			<div id="exclusive-banner">
				<?php echo get_the_post_thumbnail( $page->ID, '' ); ?>
				<div id="exclusive-range">
					<span>Select your Range</span>
					<select class="select_box range_box" name="range_product" onchange="location = this.value;">
						<option value="">Select your range</option>
						<?php foreach ((array) $terms as $term) { ?>
							<option value="<?php echo esc_attr(get_term_link($term, $taxonomy)); ?>">
								<?php echo $term->name; ?>
							</option>
						<?php } ?>
					</select>
				</div>
			</div>


			<div id="exclusive-text">	
				<?php the_content(); ?>
			</div>	
		</div>		
	</div>
    	<div class="clear"></div>
</div>
<?php } }?>  
      </section> 
 
<script src="<?php bloginfo('template_url'); ?>/js/csspopup.js"></script>
<script>
jQuery(document).ready(function(){
    jQuery("button#tog").click(function(){
        jQuery("#postbox").show("slow");
    });
});
</script>
<script>	 
	   function delete_product(prouctnuid){
		   //console.log(prouctid);
			jQuery.ajax({
					url: "<?php echo get_template_directory_uri()?>/ajax/ajax.php",
					type: "POST",
  					data: { prouctnuid : prouctnuid,act:1},
					}).done(function(result) {
						proidss = result.split("--"); 
						jQuery("#popUpDiv").show();
						jQuery(".exp_enquiry").empty();
					    jQuery(".exp_enquiry").append(proidss[0]);
						jQuery('#ajax_msg').text();
						jQuery('#ajax_msg').text(proidss[1]);
					});
	  }
	 
	 function add_product_contact(prouctid){
       console.log(prouctid);
       var cc = '.cc_'+prouctid;
       //alert(cc);
        jQuery(cc).css('pointer-events', 'none');
        jQuery(cc).attr('src','<?php echo get_template_directory_uri()?>/images/add-to-cart-light.png');
        jQuery.ajax({
                url: "<?php echo get_template_directory_uri()?>/ajax/ajax.php",
                type: "POST",
                data: { prouctid : prouctid },
                }).done(function(result) {
					proidss = result.split("--"); 
					console.log(result);
                    jQuery("#popUpDiv").show();
					jQuery(".exp_enquiry").empty();
					jQuery(".exp_enquiry").append(proidss[0]);
					jQuery('.proidsfromajax').val(proidss[2]);
					jQuery('#ajax_msg').text();
					jQuery('#ajax_msg').text(proidss[1]);
					jQuery('#show_value').text(proidss[3]);
					location.reload();
					//alert(proidss[1]);
                });
    }

    jQuery(document).ready(function() {
       
        jQuery( '.ex-exclusive_box' ).hover(
          function() {
            var product_id = jQuery(this).data('product-id');
            var mm_but = '#mm_but_'+ product_id;   
            jQuery(mm_but).show();
          }, function() {
            var product_id = jQuery(this).data('product-id');
            var mm_but = '#mm_but_'+ product_id; 
            jQuery(mm_but).hide();
          }
        );
    });  
    
    </script>
<?php get_footer(); ?>