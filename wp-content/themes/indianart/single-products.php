<?php
ob_start();
/**
 * The template for displaying all single posts
 *
 * @package WordPress
 * @subpackage Twenty_Thirteen
 * @since Twenty Thirteen 1.0
 */
session_start();
global $post;
$post_int_id = "";
get_header(); ?>
<script src="<?php bloginfo('template_url'); ?>/js/csspopup.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/jquery.bxslider.js"></script>
<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/jquery.bxslider.css">

<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/colorbox/colorbox.css">
    <script type="text/javascript" src="<?php echo get_template_directory_uri();  ?>/colorbox/jquery.colorbox-min.js"></script> 
<script>
    jQuery(document).ready(function(){
        jQuery("button#tog").click(function(){
            jQuery("#postbox").show("slow");
        });
    });
</script>
<style type="text/css">
    img.img_size{width: 100px; height: 67px !important;     padding: 6px;}
</style>

<section class="content_block_background"  id="cbb">
    <div id="page-heading">
        <h2 class="page-title"><?php the_title(); ?></h2>
        <?php if(function_exists(simple_breadcrumb)) {simple_breadcrumb();} ?>
    </div>    
    <div class="wrap pm_cumt">
        <?php while ( have_posts() ) : the_post(); ?>
        <?php $post_int_id = get_the_ID();
            $myadd_tocart = '';
            $myadd_tocart = $_SESSION['arr_exp'];
                if(in_array($post_int_id, $myadd_tocart)){
                    $inquire = 1;
                    $style = 'style="pointer-events:none;    background: #757373;"';
                }
                else
                {
                    $inquire = 0;
                    $style = '';
                }
        ?>
        <div class="single-posts">
            <div class="single-posts_left" data-product-id="<?php echo $post_int_id; ?>">
                <div class="single-post-img">
                    <?php $in_post_gal=get_post_meta($post->ID,'inpost_gallery_data',true);
                     
                    if(count($in_post_gal) == 1){
                       
                                if ( has_post_thumbnail()):
                                    the_post_thumbnail('single-featured-image');
                                endif;
                          

                    }else{ ?>
                    <ul class="bxslider">
                    <?php
                    //$in_post_gal=get_post_meta($post->ID,'inpost_gallery_data',true);
                    foreach($in_post_gal as $post_gal)
                    { ?>
<!--                         <li  rel="popupgp1" style="list-style-type:none;"><img href="<?php echo $post_gal['imgurl'];?>" class="popupgp1" src="<?php echo $post_gal['imgurl'];?>" /></li>
 -->                        <li class="img-slider"><img src="<?php echo $post_gal['imgurl'];?>" /></li>

                    <?php } ?>

                    </ul>

                    <div id="bx-pager" class="product-grid"> 
                    <?php
                    $i=0;
                    foreach($in_post_gal as $post_gal)
                    { if($i != 5){?>
                      <a data-slide-index="<?php echo $i; ?>" href=""><img src="<?php echo $post_gal['imgurl'];?>" class="img_size" /></a>
                     <?php $i++; } }?>  
                    </div>
<div class="outside">
  
  <p><span id="slider-prev"></span>  <span id="slider-next"></span></p>
</div>
                <script>
                jQuery(window).load(function(){
                    jQuery('.bxslider').bxSlider({
                        pagerCustom: '#bx-pager'

                    });

                    jQuery('.product-grid').bxSlider({
                        slideWidth: 100,
                        nextSelector: '#slider-next',
                        prevSelector: '#slider-prev',
                        auto: true,
                        controls:true,
                        randomStart:true,
                        minSlides: 2,
                        maxSlides: 4,
                        moveSlides: 1,
                        slideMargin: 10
                      });
                 });


                /*------------------------------------------------------------------------*/

                jQuery(document).ready(function(){
                //Examples of how to assign the Colorbox event to elements
                jQuery(".popupgp1").colorbox({rel:'popupgp1',transition:"none" ,width:"50%", height:"60%"});
            
                
                
            });
                </script>
<?php } ?>
                <div class="single-inqury-btn">
                    <!--Single page inqirey button -->
                        <div id="mm_but_<?php echo $post_int_id;?>" class="but_mm-single" style="display:none;">
                            <a class="cc_<?php echo $post_int_id;?>" <?php echo $style; ?> onclick="add_product_contact('<?php echo $post_int_id;?>')" class="exp_int">
                                <?php if($inquire == 1){_e('Enquired');}else{ _e('Enquire');} ?>
                            </a>
                        </div>
                    <!--Single page inqirey button -->
                </div>

                </div>

 


            </div>
            
            <div class="single-posts_right">
                <div class="product-feature">



                    <div class="product-row">
                        <div class="product-col">
                            <?php _e('Product Code :'); ?>
                        </div>
                        <div class="product-col">
                            <?php the_field('product_code'); ?>
                        </div>
                    </div>
                    <div class="product-row">
                        <div class="product-col">
                            <?php _e('Material :'); ?>
                        </div>
                        <div class="product-col">
                            <?php the_field('material'); ?>
                        </div>
                    </div>
                    <div class="product-row">
                        <div class="product-col">
                            <?php _e('Measurement :'); ?>
                        </div>
                        <div class="product-col">
                            <?php the_field('measurement'); ?>
                        </div>
                    </div>
                    <div class="product-row">
                        <div class="product-col">
                            <?php _e('Description :'); ?>
                        </div>
                        <div class="product-col product-desc">
                            <?php the_field('description'); ?>
                        </div>
                    </div>
                    <div class="product-row">
                        <div class="product-col">Price :</div>
                        <div class="product-col" id="price">
                            <?php 
                            $amount = get_field( "price", $post_int_id );
                            if($_SESSION['currency'] == ''){echo the_field('price');}else{
                                echo converCurrency('INR', $_SESSION['currency'], $amount); } ?>
                        </div>
                        <?php $selected ='selected'; ?>
                        <div id="curren">
                            <select id="currency" class="select_box inr_box">
                              <option <?php if($_SESSION['currency'] == "INR"){echo $selected;} ?> value="INR">INR</option>
                              <option <?php if($_SESSION['currency'] == "USD"){echo $selected;} ?> value="USD">USD</option>
                              <option <?php if($_SESSION['currency'] == "EUR"){echo $selected;} ?> value="EUR">EUR</option>
                              <option <?php if($_SESSION['currency'] == "AUD"){echo $selected;} ?> value="AUD">AUD</option>
                              <option <?php if($_SESSION['currency'] == "CAD"){echo $selected;} ?> value="CAD">CAD</option>
                              <option <?php if($_SESSION['currency'] == "SGD"){echo $selected;} ?> value="SGD">SGD</option>
                            </select>   
                        </div>
                    </div>
                </div>
                <div class="single-content">
                    <?php the_content(); ?>
                </div>
        		<div class="clear"></div>
            </div>
            <?php endwhile; ?>
        </div>
    </div>    
</section>
<div id="blanket" style="display:none;"></div>
    <div id="popUpDiv" style="display:none;">
        <a onclick="popup('popUpDiv')" class="closeenq" title="close">
        <!-- <?php //_e('X',''); ?> -->
            
        </a>
        <div class="content-express">
            <h2><?php _e('Yours selected product has been added to Enquiry list.'); ?></h2>
            <p style="color:#666666; font:22px 'Century Gothic';">
                <?php _e('Please visit <strong>Contact Us</strong> page to enter details.',''); ?> 
            </p>

            <div id="ajax_msg"></div>
            <div class="two_buttons">
                <a onclick="popup('popUpDiv')">
                    <!-- <div class="enq-buttons_cont">
                        <?php //_e('Ok',''); ?>
                    </div> -->
                </a>
                <div class="clear"></div>
            </div>
        </div>
    </div>
<script>	 
	   function delete_product(prouctnuid){
		   //console.log(prouctid);
			jQuery.ajax({
					url: "<?php echo get_template_directory_uri()?>/ajax/ajax.php",
					type: "POST",
  					data: { prouctnuid : prouctnuid,act:1},
					}).done(function(result) {
						proidss = result.split("--"); 
						jQuery("#popUpDiv").show();
						jQuery(".exp_enquiry").empty();
					    jQuery(".exp_enquiry").append(proidss[0]);
						jQuery('#ajax_msg').text();
						jQuery('#ajax_msg').text(proidss[1]);
					});
	  }
	 
	   function add_product_contact(prouctid){
           console.log(prouctid);
           var cc = '.cc_'+prouctid;
           //alert(cc);
            jQuery(cc).css('pointer-events', 'none');
            jQuery(cc).attr('src','<?php echo get_template_directory_uri()?>/images/add-to-cart-light.add-to-cart-light');
            jQuery.ajax({
                    url: "<?php echo get_template_directory_uri()?>/ajax/ajax.php",
                    type: "POST",
                    data: { prouctid : prouctid },
                    }).done(function(result) {
    					proidss = result.split("--"); 
    					console.log(result);
                        jQuery("#popUpDiv").show();
    					jQuery(".exp_enquiry").empty();
    					jQuery(".exp_enquiry").append(proidss[0]);
    					jQuery('.proidsfromajax').val(proidss[2]);
    					jQuery('#ajax_msg').text();
    					jQuery('#ajax_msg').text(proidss[1]);
                        jQuery('#show_value').text(proidss[3]);
                        location.reload();
    					//alert(proidss[1]);
                    });
    }
    jQuery( "#curren" ).click(function() {
        var curren = jQuery( "#currency" ).val();
        jQuery.ajax({
                url: "<?php echo get_template_directory_uri()?>/ajax/change.php",
                type: "POST",
                data: { curren : curren},
                }).done(function(result) {
                    console.log(result);
                    location.reload();
                });
    });
    jQuery(document).ready(function() {
        jQuery( '.single-posts_left' ).hover(
          function() {
            var product_id = jQuery(this).data('product-id');
            var mm_but = '#mm_but_'+ product_id;   
            jQuery(mm_but).show();
          }, function() {
            var product_id = jQuery(this).data('product-id');
            var mm_but = '#mm_but_'+ product_id; 
            jQuery(mm_but).hide();
          }
        );
    });  
</script>




<?php //get_sidebar(); ?>
<?php get_footer(); ?>